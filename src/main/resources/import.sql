﻿-- /* Populate tabla clientes */

INSERT INTO pacientes (nombres_paciente, apellidos_paciente, dni_paciente, telefono_paciente, email_paciente, create_at_paciente) VALUES ('Cristian Camilo', 'Donoso Cubillos', '17278336', '942380004', 'cd@gmail.com', '2019-01-01' );
INSERT INTO pacientes (nombres_paciente, apellidos_paciente, dni_paciente, telefono_paciente, email_paciente, create_at_paciente) VALUES ('Valentina Ignacia', 'Donoso Luna', '17278337', '942380005', 'vi@gmail.com', '2019-01-01' );
INSERT INTO pacientes (nombres_paciente, apellidos_paciente, dni_paciente, telefono_paciente, email_paciente, create_at_paciente) VALUES ('Bongo Patricio', 'Donoso Cubillos', '17278338', '942380006', 'bongo@gmail.com', '2019-01-01' );
INSERT INTO pacientes (nombres_paciente, apellidos_paciente, dni_paciente, telefono_paciente, email_paciente, create_at_paciente) VALUES ('Vincent', 'Valentine', '17278339', '942380009', 'vv@gmail.com', '2019-01-01' );

-- /* Creamos algunos usuarios con sus roles */
INSERT INTO usuarios (id, username, password, enabled, nombre, apellido, email) VALUES (1, 'cristian','$2a$10$9I9r4BttoBIJL7p6m4ht0eIvVi7u9KyUNEIZFq5ZkgcJdlSk89Pj2', true, 'Cristian', 'Donoso','cd@gmail.com');   
INSERT INTO usuarios (id, username, password, enabled, nombre, apellido, email) VALUES (2, 'admin','$2a$10$RmdEsvEfhI7Rcm9f/uZXPebZVCcPC7ZXZwV51efAvMAp1rIaRAfPK', true, 'Admin', 'Admin','adm@gmail.com');

INSERT INTO roles (nombre) VALUES ('ROLE_USER');
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (1, 1);
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2, 2);
INSERT INTO usuarios_roles (usuario_id, role_id) VALUES (2, 1);
