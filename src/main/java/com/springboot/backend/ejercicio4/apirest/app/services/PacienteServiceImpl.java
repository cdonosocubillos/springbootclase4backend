package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;
import com.springboot.backend.ejercicio4.apirest.app.repository.IPacienteRepository;

@Service
public class PacienteServiceImpl implements IPacienteService{

	@Autowired
	IPacienteRepository pacienteRepo;
	
	@Override
	@Transactional
	public Paciente save(Paciente pac) {
		return pacienteRepo.save(pac);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Paciente> findAll() {
		return pacienteRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Paciente findById(Long idPac) {
		return pacienteRepo.findById(idPac).orElse(null);
	}

	@Override
	@Transactional
	public boolean delete(Long idPac) {
		if (idPac != null) {
			pacienteRepo.deleteById(idPac);
			return true;
		}else {
			return false;
		}
	}

}
