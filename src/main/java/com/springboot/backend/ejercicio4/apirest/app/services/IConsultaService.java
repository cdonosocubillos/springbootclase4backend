package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;

public interface IConsultaService {

	public Consulta save(Consulta consulta);
	public List<Consulta> findAll();
	public Consulta findById(Long idConsulta);
	public boolean delete(Long idConsulta);
}
