package com.springboot.backend.ejercicio4.apirest.app.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;

@Repository
public interface IPacienteRepository extends JpaRepository<Paciente, Long>{

}
