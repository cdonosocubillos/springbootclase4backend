package com.springboot.backend.ejercicio4.apirest.app.services;

import com.springboot.backend.ejercicio4.apirest.app.entity.Usuario;

public interface IUsuarioService { //interfaz personalizada

	public Usuario findByUsername(String username);
}
