package com.springboot.backend.ejercicio4.apirest.app.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;
import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;
import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;
import com.springboot.backend.ejercicio4.apirest.app.repository.IMedicoRepository;
import com.springboot.backend.ejercicio4.apirest.app.repository.IPacienteRepository;
import com.springboot.backend.ejercicio4.apirest.app.services.IConsultaService;
import com.springboot.backend.ejercicio4.apirest.app.services.IDetalleConsultaService;
import com.springboot.backend.ejercicio4.apirest.app.services.IEspecialidadService;
import com.springboot.backend.ejercicio4.apirest.app.services.IMedicoService;
import com.springboot.backend.ejercicio4.apirest.app.services.IPacienteService;
import com.springboot.backend.ejercicio4.apirest.app.wrappers.ConsultaWrapper;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class ConsultaRestController {
	
//	private final Logger log = LoggerFactory.getLogger(PacienteRestController.class);
	
	@Autowired
	private IConsultaService consultaService;
	
	@Autowired 
	private IEspecialidadService especialidadService;
	
	@Autowired
	private IMedicoService medicoService;
	
	@Autowired
	private IPacienteService pacienteService;
	
	@Autowired
	private IDetalleConsultaService detalleConsultaService;
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/consultas")
	public List<Consulta> findAll() {
		return consultaService.findAll();
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/consultas/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		Consulta consulta = null;
		Map<String, Object> response = new HashMap<>();
		try {
			consulta = consultaService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(consulta==null) {
			response.put("mensaje", "La Consulta ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Consulta>(consulta, HttpStatus.OK);
	}
	
//	@Secured("ROLE_ADMIN")
//	@PostMapping("/consultas")
//	public ResponseEntity<?> create(@Valid @RequestBody Consulta consulta, BindingResult result) {
//		
//		Consulta consultaNew = null;
//		Map<String, Object> response = new HashMap<>();
//		
//		if(result.hasErrors()) {
//
//			List<String> errors = result.getFieldErrors()
//					.stream()
//					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
//					.collect(Collectors.toList());
//			
//			response.put("errors", errors);
//			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
//		}
//		
//		try {
//			consulta.setCreateAt(LocalDateTime.now());
//			consultaNew = consultaService.save(consulta);
//		} catch(DataAccessException e) {
//			response.put("mensaje", "Error al realizar el insert en la base de datos");
//			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
//			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		
//		response.put("mensaje", "La Consulta ha sido creada con éxito!");
//		response.put("consulta", consultaNew);
//		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
//	}
	
	@Secured("ROLE_ADMIN")
	@PostMapping("/consultas")
	public ResponseEntity<?> create(@Valid @RequestBody Consulta consulta, BindingResult result) {
		
		Consulta consultaNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			consulta.setCreateAt(LocalDateTime.now());
			consulta.setMedico(medicoService.findById(consulta.getMedico().getIdMedico()));
			consulta.setPaciente(pacienteService.findById(consulta.getPaciente().getIdPaciente()));
			consulta.setEspecialidad(especialidadService.findById(consulta.getEspecialidad().getIdEspecialidad()));
			consulta.getDetalleConsulta().stream().forEach(det -> { det.setConsulta(consulta); });
			consultaNew = consultaService.save(consulta);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "La Consulta ha sido creada con éxito!");
		response.put("consulta", consultaNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}

//	WRAPPER
//	@Secured("ROLE_ADMIN")
//	@PostMapping("/consultas")
//	public ResponseEntity<?> create(@Valid @RequestBody ConsultaWrapper consultaWrapper, BindingResult result) {
//		
//		Consulta consulta = consultaWrapper.getConsulta();
//		Consulta consultaNew = null;
//		List<DetalleConsulta> detalleConsulta = consulta.getDetalleConsulta();
//		Especialidad especialidad = null;
//		Medico medico = null;
//		Paciente paciente = null;
//		Map<String, Object> response = new HashMap<>();
//		
//		if(result.hasErrors()) {
//
//			List<String> errors = result.getFieldErrors()
//					.stream()
//					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
//					.collect(Collectors.toList());
//			
//			response.put("errors", errors);
//			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
//		}
//		
//		try {
//			/*Tomando una consulta*/
//			/* Detalles de la consulta*/
//			detalleConsulta.stream().forEach(det -> {det.setConsulta(consulta);});
//			consulta.setDetalleConsulta(detalleConsulta);
//			/*Especialidad*/
//			especialidad = especialidadService.findById(consultaWrapper.getIdEspecialidad());
//			consulta.setEspecialidad(especialidad);
//			/*Medico*/
//			medico = medicoService.findById(consultaWrapper.getIdMedico());
//			consulta.setMedico(medico);
//			/*Paciente*/
//			paciente = pacienteService.findById(consultaWrapper.getIdPac());
//			consulta.setPaciente(paciente);
//			/**/
//			consulta.setCreateAt(LocalDateTime.now());
//			consultaNew = consultaService.save(consulta);
//		} catch(DataAccessException e) {
//			response.put("mensaje", "Error al realizar el insert en la base de datos");
//			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
//			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//		
//		response.put("mensaje", "La Consulta ha sido creada con éxito!");
//		response.put("consulta", consultaNew);
//		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
//	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/consultas/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Consulta consulta, BindingResult result, @PathVariable Long id) {

		Consulta consultaActual = consultaService.findById(id);

		Consulta consultaUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (consultaActual == null) {
			response.put("mensaje", "Error: no se pudo editar, la consulta ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {
			consultaActual.setCreateAt(LocalDateTime.now());
			consultaUpdated = consultaService.save(consultaActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "La Consulta ha sido actualizada con éxito!");
		response.put("medico", consultaUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/consultas/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			Consulta consulta = consultaService.findById(id);
			especialidadService.delete(consulta.getIdConsulta());
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar la consulta de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Consulta eliminada con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	

}
