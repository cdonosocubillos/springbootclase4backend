package com.springboot.backend.ejercicio4.apirest.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "medicos")
public class Medico implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMedico;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 70, message = "Excede el máximo de 70 caracteres")
	@Column(name = "nombres_medico")
	private String nombres;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 70, message = "Excede el máximo de 70 caracteres")
	@Column(name = "apellidos_medico")
	private String apellidos;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 12, message = "Excede el máximo de 12 caracteres")
	@Column(name = "cmp_medico")
	private String CMP;
	
	@NotNull(message = "No puede ser nulo")
	@Column(name = "foto_url_medico")
	private String fotoUrl;
	
	@Column(name="create_at_medico")
	private LocalDateTime createAt;

	public Long getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(Long idMedico) {
		this.idMedico = idMedico;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCMP() {
		return CMP;
	}

	public void setCMP(String cMP) {
		CMP = cMP;
	}

	public String getFotoUrl() {
		return fotoUrl;
	}

	public void setFotoUrl(String fotoUrl) {
		this.fotoUrl = fotoUrl;
	}

	public LocalDateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}
}
