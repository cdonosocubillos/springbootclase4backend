package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;
import com.springboot.backend.ejercicio4.apirest.app.repository.IMedicoRepository;

@Service
public class MedicoServiceImpl implements IMedicoService{

	@Autowired
	IMedicoRepository medicoRepo;
	
	@Override
	@Transactional
	public Medico save(Medico medico) {
		return medicoRepo.save(medico);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Medico> findAll() {
		return medicoRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Medico findById(Long idMedico) {
		return medicoRepo.getOne(idMedico);
	}

	@Override
	@Transactional
	public boolean delete(Long idMedico) {
		if (idMedico != null) {
			medicoRepo.deleteById(idMedico);
			return true;
		}else {
			return false;
		}
	}

}
