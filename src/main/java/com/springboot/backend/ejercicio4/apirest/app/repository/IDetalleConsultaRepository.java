package com.springboot.backend.ejercicio4.apirest.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;
import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;
import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;

public interface IDetalleConsultaRepository extends JpaRepository<DetalleConsulta, Long>{

	@Query("from Consulta")
	public List<Consulta> getAllConsultas();
}
