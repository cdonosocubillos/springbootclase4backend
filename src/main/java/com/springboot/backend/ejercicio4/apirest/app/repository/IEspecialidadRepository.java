package com.springboot.backend.ejercicio4.apirest.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;

public interface IEspecialidadRepository extends JpaRepository<Especialidad, Long>{

}
