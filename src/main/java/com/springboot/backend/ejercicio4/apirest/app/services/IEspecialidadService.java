package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;

public interface IEspecialidadService {

	public Especialidad save(Especialidad Especialidad);
	public List<Especialidad> findAll();
	public Especialidad findById(Long idEspecialidad);
	public boolean delete(Long idEspecialidad);
}
