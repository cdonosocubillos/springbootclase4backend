package com.springboot.backend.ejercicio4.apirest.app.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.springboot.backend.ejercicio4.apirest.app.entity.Usuario;


public interface IUsuarioRepository extends CrudRepository<Usuario, Long>{
	
	public Usuario findByUsername(String username); //encontrar usuario
	
	@Query("select u from Usuario u where u.username=?1") //encontrar usuario con metodo personalizado sin la palabra reservada
	public Usuario findByUsername2(String username);

}
