package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;
import com.springboot.backend.ejercicio4.apirest.app.repository.IDetalleConsultaRepository;

@Service
public class DetalleConsultaServiceImpl implements IDetalleConsultaService{

	@Autowired
	IDetalleConsultaRepository detalleConsultaRepo;
	
	@Override
	@Transactional
	public DetalleConsulta save(DetalleConsulta detalleConsulta) {
		return detalleConsultaRepo.save(detalleConsulta);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DetalleConsulta> findAll() {
		return detalleConsultaRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public DetalleConsulta findById(Long idDetalleConsulta) {
		return detalleConsultaRepo.getOne(idDetalleConsulta);
	}

	@Override
	@Transactional
	public boolean delete(Long idDetalleConsulta) {
		if (idDetalleConsulta != null) {
			detalleConsultaRepo.deleteById(idDetalleConsulta);
			return true;
		}else {
			return false;
		}
	}

}
