package com.springboot.backend.ejercicio4.apirest.app.controllers;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;
import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;
import com.springboot.backend.ejercicio4.apirest.app.services.IMedicoService;
import com.springboot.backend.ejercicio4.apirest.app.services.IPacienteService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
public class MedicoRestController {
	
//	private final Logger log = LoggerFactory.getLogger(PacienteRestController.class);
	
	@Autowired
	private IMedicoService medicoService;
	
	@Secured({"ROLE_ADMIN"})
	@GetMapping("/medicos")
	public List<Medico> findAll() {
		return medicoService.findAll();
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_USER"})
	@GetMapping("/medicos/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		Medico medico = null;
		Map<String, Object> response = new HashMap<>();
		try {
			medico = medicoService.findById(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la consulta en la base de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(medico==null) {
			response.put("mensaje", "El medico ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Medico>(medico, HttpStatus.OK);
	}

	@Secured("ROLE_ADMIN")
	@PostMapping("/medicos")
	public ResponseEntity<?> create(@Valid @RequestBody Medico medico, BindingResult result) {
		
		Medico medicoNew = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			medico.setCreateAt(LocalDateTime.now());
			medicoNew = medicoService.save(medico);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el insert en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "El medico ha sido creado con éxito!");
		response.put("medico", medicoNew);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping("/medicos/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Medico medico, BindingResult result, @PathVariable Long id) {

		Medico medicoActual = medicoService.findById(id);

		Medico medicoUpdated = null;

		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (medicoActual == null) {
			response.put("mensaje", "Error: no se pudo editar, al medico ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			medicoActual.setApellidos(medico.getApellidos());
			medicoActual.setNombres(medico.getNombres());
			medicoActual.setCMP(medico.getCMP());
			medicoActual.setFotoUrl(medico.getFotoUrl());
			medicoActual.setCreateAt(LocalDateTime.now());

			medicoUpdated = medicoService.save(medicoActual);

		} catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar al medico en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		response.put("mensaje", "El medico ha sido actualizado con éxito!");
		response.put("medico", medicoUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@Secured("ROLE_ADMIN")
	@DeleteMapping("/medicos/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			Medico medico = medicoService.findById(id);
			medicoService.delete(medico.getIdMedico());
		} catch (DataAccessException e) {
			response.put("mensaje", "Error al eliminar al medico de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje", "Medico eliminado con éxito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	

}
