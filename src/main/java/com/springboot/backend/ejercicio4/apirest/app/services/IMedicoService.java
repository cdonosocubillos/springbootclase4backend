package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;

public interface IMedicoService {

	public Medico save(Medico medico);
	public List<Medico> findAll();
	public Medico findById(Long idMedico);
	public boolean delete(Long idMedico);
}
