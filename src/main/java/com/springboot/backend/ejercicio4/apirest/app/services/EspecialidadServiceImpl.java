package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;
import com.springboot.backend.ejercicio4.apirest.app.repository.IEspecialidadRepository;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService{

	
	@Autowired
	IEspecialidadRepository especialidadRepo;
	
	@Override
	@Transactional
	public Especialidad save(Especialidad Especialidad) {
		return especialidadRepo.save(Especialidad);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Especialidad> findAll() {
		return especialidadRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Especialidad findById(Long idEspecialidad) {
		return especialidadRepo.getOne(idEspecialidad);
	}

	@Override
	@Transactional
	public boolean delete(Long idEspecialidad) {
		if (idEspecialidad != null) {
			especialidadRepo.deleteById(idEspecialidad);
			return true;
		}else {
			return false;
		}
	}

}
