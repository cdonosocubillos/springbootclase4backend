package com.springboot.backend.ejercicio4.apirest.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.Especialidad;
import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;
import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;

public interface IConsultaRepository extends JpaRepository<Consulta, Long>{

	@Query("from Medico")
	public List<Medico> getAllMedicos();
	@Query("from Paciente")
	public List<Paciente> getAllPacientes();
	@Query("from Especialidad")
	public List<Especialidad> getAllEspecialidades();
	@Query("from DetalleConsulta")
	public List<DetalleConsulta> getAllDetallesConsultas();
}
