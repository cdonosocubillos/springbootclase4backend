package com.springboot.backend.ejercicio4.apirest.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "examenes")
public class Examen implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long idExamen;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 50, message = "Excede el máximo de 50 caracteres")
	@Column(name = "nombre_examen")
	private String nombre;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 150, message = "Excede el máximo de 150 caracteres")
	@Column(name = "descripcion_examen")
	private String descripcion;

	public Long getIdExamen() {
		return idExamen;
	}

	public void setIdExamen(Long idExamen) {
		this.idExamen = idExamen;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
