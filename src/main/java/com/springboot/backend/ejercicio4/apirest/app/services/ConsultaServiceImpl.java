package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;
import com.springboot.backend.ejercicio4.apirest.app.repository.IConsultaRepository;

@Service
public class ConsultaServiceImpl implements IConsultaService{

	@Autowired
	IConsultaRepository consultaRepo;
	
	@Override
	@Transactional
	public Consulta save(Consulta consulta) {
		return consultaRepo.save(consulta);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Consulta> findAll() {
		return consultaRepo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Consulta findById(Long idConsulta) {
		return consultaRepo.getOne(idConsulta);
	}

	@Override
	@Transactional
	public boolean delete(Long idConsulta) {
		if (idConsulta != null) {
			consultaRepo.deleteById(idConsulta);
			return true;
		}else {
			return false;
		}
	}

}
