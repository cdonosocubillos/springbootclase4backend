package com.springboot.backend.ejercicio4.apirest.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "menus")
public class Menu implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idMenu")
	private Long idMenu;
	
	@Length(max = 20, message = "Excede el máximo de 20 caracteres")
	@Column(name = "icono_menu")
	private String icono;
	
	@Length(max = 20, message = "Excede el máximo de 20 caracteres")
	@Column(name = "nombre_menu")
	private String nombre;
	
	@Length(max = 20, message = "Excede el máximo de 20 caracteres")
	@Column(name = "url_menu")
	private String url;

	public Long getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Long idMenu) {
		this.idMenu = idMenu;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
