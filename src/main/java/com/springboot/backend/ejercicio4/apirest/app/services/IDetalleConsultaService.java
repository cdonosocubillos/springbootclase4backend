package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;

public interface IDetalleConsultaService {

	public DetalleConsulta save(DetalleConsulta detalleConsulta);
	public List<DetalleConsulta> findAll();
	public DetalleConsulta findById(Long idDetalleConsulta);
	public boolean delete(Long idDetalleConsulta);
}
