package com.springboot.backend.ejercicio4.apirest.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.backend.ejercicio4.apirest.app.entity.Medico;

public interface IMedicoRepository extends JpaRepository<Medico, Long>{

}
