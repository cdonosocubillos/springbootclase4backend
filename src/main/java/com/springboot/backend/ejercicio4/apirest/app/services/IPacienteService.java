package com.springboot.backend.ejercicio4.apirest.app.services;

import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.Paciente;

public interface IPacienteService {
	
	public Paciente save(Paciente pac);
	public List<Paciente> findAll();
	public Paciente findById(Long idPac);
	public boolean delete(Long idPac);

}
