package com.springboot.backend.ejercicio4.apirest.app.wrappers;

import java.io.Serializable;
import java.util.List;

import com.springboot.backend.ejercicio4.apirest.app.entity.Consulta;
import com.springboot.backend.ejercicio4.apirest.app.entity.DetalleConsulta;

public class ConsultaWrapper implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Consulta consulta;
	private List<DetalleConsulta> detalleConsulta;
	private Long idMedico;
	private Long idPac;
	private Long idEspecialidad;
	
	public Consulta getConsulta() {
		return consulta;
	}
	public void setConsulta(Consulta consulta) {
		this.consulta = consulta;
	}
	public Long getIdMedico() {
		return idMedico;
	}
	public void setIdMedico(Long idMedico) {
		this.idMedico = idMedico;
	}
	public Long getIdPac() {
		return idPac;
	}
	public void setIdPac(Long idPac) {
		this.idPac = idPac;
	}
	public Long getIdEspecialidad() {
		return idEspecialidad;
	}
	public void setIdEspecialidad(Long idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}
	public List<DetalleConsulta> getDetalleConsulta() {
		return detalleConsulta;
	}
	public void setDetalleConsulta(List<DetalleConsulta> detalleConsulta) {
		this.detalleConsulta = detalleConsulta;
	}
	
	
	

}
