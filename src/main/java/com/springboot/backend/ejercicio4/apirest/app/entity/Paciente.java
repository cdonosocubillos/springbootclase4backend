package com.springboot.backend.ejercicio4.apirest.app.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

@Entity
@Table(name = "pacientes")
public class Paciente implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPaciente;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 70)
	@Column(name = "nombres_paciente")
	private String nombres;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 70, message = "Excede el máximo de 70 caracteres")
	@Column(name = "apellidos_paciente")
	private String apellidos;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 8, message = "Excede el máximo de 8 caracteres")
	@Column(name = "dni_paciente", unique = true)
	private String dni;
	
	@NotNull(message = "No puede ser nulo")
	@Length(max = 9, message = "Excede el máximo de 9 caracteres")
	@Column(name = "telefono_paciente")
	private String telefono;
	
	@NotNull(message = "No puede ser nulo")
	@Email()
	@Length(max = 55, message = "Excede el máximo de 55 caracteres")
	@Column(unique=true, name = "email_paciente")
	private String email;
	
	@Column(name="create_at_paciente")
	private LocalDateTime createAt;

	public Long getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Long idPaciente) {
		this.idPaciente = idPaciente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}
}
